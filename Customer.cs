﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace QuickBooksIntegrationDemo
{
    public class Customer : ModelBase
    {
        private string name;
        private string quickBooksId;
        private string editSequence;

        public string Name 
        { 
            get { return name; }
            set
            {
                if (object.ReferenceEquals(name, value) != true)
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public string QuickBooksID 
        {
            get { return quickBooksId; }
            set
            {
                if (object.ReferenceEquals(quickBooksId, value) != true)
                {
                    quickBooksId = value;
                    NotifyPropertyChanged("QuickBooksID");                        
                }
            }
        }

        public string EditSequence 
        {
            get { return editSequence; }
            set
            {
                if (object.ReferenceEquals(editSequence, value) != true)
                {
                    editSequence = value;
                    NotifyPropertyChanged("EditSequence");
                }
            }
        }
    }
}
