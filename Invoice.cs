﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace QuickBooksIntegrationDemo
{
    public class Invoice : ModelBase
    {
        private Customer customer;
        private ObservableCollection<InvoiceItem> invoiceItems = new ObservableCollection<InvoiceItem>();
        private string quickBooksId;
        private string editSequence;

        public Customer Customer 
        {
            get { return customer; }
            set
            {
                if (object.ReferenceEquals(customer, value) != true)
                {
                    customer = value;
                    NotifyPropertyChanged("Customer");
                }
            }
        }

        public ObservableCollection<InvoiceItem> InvoiceItems 
        {
            get { return invoiceItems; }
            set
            {
                if (object.ReferenceEquals(invoiceItems, value) != true)
                {
                    invoiceItems = value;
                    NotifyPropertyChanged("InvoiceItems");
                }
            }
        }

        public string QuickBooksID
        {
            get { return quickBooksId; }
            set
            {
                if (object.ReferenceEquals(quickBooksId, value) != true)
                {
                    quickBooksId = value;
                    NotifyPropertyChanged("QuickBooksID");
                }
            }
        }

        public string EditSequence
        {
            get { return editSequence; }
            set
            {
                if (object.ReferenceEquals(editSequence, value) != true)
                {
                    editSequence = value;
                    NotifyPropertyChanged("EditSequence");
                }
            }
        }
    }
}
