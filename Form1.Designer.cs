﻿namespace QBwin
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dtb = new System.Windows.Forms.DateTimePicker();
            this.dte = new System.Windows.Forms.DateTimePicker();
            this.To = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.displayBtn = new System.Windows.Forms.Button();
            this.send = new System.Windows.Forms.Button();
            this.dtg = new System.Windows.Forms.DataGridView();
            this.checka = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.selectall = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dtb
            // 
            this.dtb.CustomFormat = "MM/dd/yyyy";
            this.dtb.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtb.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtb.Location = new System.Drawing.Point(8, 19);
            this.dtb.Name = "dtb";
            this.dtb.Size = new System.Drawing.Size(103, 21);
            this.dtb.TabIndex = 0;
            // 
            // dte
            // 
            this.dte.CustomFormat = "MM/dd/yyyy";
            this.dte.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte.Location = new System.Drawing.Point(125, 19);
            this.dte.Name = "dte";
            this.dte.Size = new System.Drawing.Size(101, 21);
            this.dte.TabIndex = 1;
            // 
            // To
            // 
            this.To.AutoSize = true;
            this.To.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.To.Location = new System.Drawing.Point(5, 3);
            this.To.Name = "To";
            this.To.Size = new System.Drawing.Size(53, 13);
            this.To.TabIndex = 2;
            this.To.Text = "Start Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(122, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "End Date";
            // 
            // displayBtn
            // 
            this.displayBtn.BackColor = System.Drawing.Color.Teal;
            this.displayBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.displayBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.displayBtn.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.displayBtn.ForeColor = System.Drawing.Color.White;
            this.displayBtn.Location = new System.Drawing.Point(257, 17);
            this.displayBtn.Name = "displayBtn";
            this.displayBtn.Size = new System.Drawing.Size(130, 23);
            this.displayBtn.TabIndex = 4;
            this.displayBtn.Text = "Display UnBilled Orders";
            this.displayBtn.UseVisualStyleBackColor = false;
            this.displayBtn.Click += new System.EventHandler(this.displayBtn_Click);
            // 
            // send
            // 
            this.send.BackColor = System.Drawing.Color.DarkKhaki;
            this.send.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.send.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.send.Location = new System.Drawing.Point(393, 17);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(149, 23);
            this.send.TabIndex = 5;
            this.send.Text = "Send Selected Orders To QB";
            this.send.UseVisualStyleBackColor = false;
            this.send.Visible = false;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // dtg
            // 
            this.dtg.AllowUserToOrderColumns = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtg.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dtg.BackgroundColor = System.Drawing.Color.White;
            this.dtg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dtg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checka});
            this.dtg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtg.GridColor = System.Drawing.Color.LightGray;
            this.dtg.Location = new System.Drawing.Point(0, 0);
            this.dtg.MultiSelect = false;
            this.dtg.Name = "dtg";
            this.dtg.Size = new System.Drawing.Size(1800, 763);
            this.dtg.TabIndex = 6;
            // 
            // checka
            // 
            this.checka.HeaderText = "...";
            this.checka.Name = "checka";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.selectall);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel1.Controls.Add(this.send);
            this.splitContainer1.Panel1.Controls.Add(this.displayBtn);
            this.splitContainer1.Panel1.Controls.Add(this.To);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.dte);
            this.splitContainer1.Panel1.Controls.Add(this.dtb);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dtg);
            this.splitContainer1.Size = new System.Drawing.Size(1800, 810);
            this.splitContainer1.SplitterDistance = 45;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(551, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // selectall
            // 
            this.selectall.AutoSize = true;
            this.selectall.Checked = true;
            this.selectall.CheckState = System.Windows.Forms.CheckState.Checked;
            this.selectall.Location = new System.Drawing.Point(234, 22);
            this.selectall.Name = "selectall";
            this.selectall.Size = new System.Drawing.Size(15, 14);
            this.selectall.TabIndex = 7;
            this.selectall.UseVisualStyleBackColor = true;
            this.selectall.Visible = false;
            this.selectall.CheckedChanged += new System.EventHandler(this.selectall_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1800, 810);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QB/Miff Orders Manager";
            ((System.ComponentModel.ISupportInitialize)(this.dtg)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtb;
        private System.Windows.Forms.DateTimePicker dte;
        private System.Windows.Forms.Label To;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button displayBtn;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.DataGridView dtg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checka;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox selectall;
    }
}

