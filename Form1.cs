﻿using QBFC10Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.IO;

namespace QBwin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SGBD sg = new SGBD(System.Configuration.ConfigurationManager.AppSettings["cnx"]);

        public DataTable dtL(string dtb, string dte)
        {
            string sql = @"EXEC get_unbilled_orders '" + dtb + "','" + dte + "'";
            return sg.getTableByCommande(sql);
        }

        private void displayBtn_Click(object sender, EventArgs e)
        {
            send.Visible = false;
            pictureBox1.Visible = false;
            selectall.Visible = true;
            bool dtOK = false;
            if (dtb.Value == null)
            {
                MessageBox.Show("Please select a Begin date");
                dtOK = false;
            }
            else
            {
                dtOK = true;
            }

            if (dtb.Value == null)
            {
                MessageBox.Show("Please select an End date");
                dtOK = false;
            }
            else
            {
                dtOK = true;
            }

            if (dtOK)
            {
                dtg.DataSource = dtL(dtb.Value.ToString("yyyy-MM-dd"), dte.Value.ToString("yyyy-MM-dd")).AsDataView();

                if (dtg.Rows.Count > 0)
                {
                    send.Visible = true;
                    pictureBox1.Visible = true;
                }


            }


            for (int i = 0; i < dtg.Rows.Count; i++)
            {
                dtg.Rows[i].Cells[0].Value = true;

            }


        }

        private void send_Click(object sender, EventArgs e)
        {
            displayBtn.Enabled = false;
            dtg.Enabled = false;
            send.Visible = false;
            for (int i = 0; i < dtg.Rows.Count; i++)
            {

                if (dtg.Rows[i].Cells[0].Value != null && dtg.Rows[i].Cells[0].Value.ToString() == "True" && dtg.Rows[i].Cells[1].Value != null)
                {
                    // dtg.Rows[i].Cells[11].Value = "Processed [x]";

                    bool sessionBegun = false;
                    bool connectionOpen = false;
                    QBSessionManager sessionManager = null;

                    try
                    {
                        //Create the session Manager object
                        sessionManager = new QBSessionManager();

                        //Create the message set request object to hold our request
                        IMsgSetRequest requestMsgSet = sessionManager.CreateMsgSetRequest("US", 8, 0);
                        requestMsgSet.Attributes.OnError = ENRqOnError.roeContinue;

                        //Connect to QuickBooks and begin a session
                        sessionManager.OpenConnection(System.Configuration.ConfigurationManager.AppSettings["qbFile"], "QuickBooks Integration");
                        connectionOpen = true;
                        sessionManager.BeginSession("", ENOpenMode.omDontCare);
                        sessionBegun = true;


                        string vendor = dtg.Rows[i].Cells[1].Value.ToString();
                        string customer = dtg.Rows[i].Cells[2].Value.ToString();
                        string account = dtg.Rows[i].Cells[3].Value.ToString();
                        string amount = dtg.Rows[i].Cells[4].Value.ToString();

                        IBillAdd BillAddRq = null;
                        IExpenseLineAdd Expense = null;

                        IResponse response = null;
                        IBillRet billRet = null;
                        string msg = String.Empty;


                        BillAddRq = requestMsgSet.AppendBillAddRq();

                        BillAddRq.TxnDate.SetValue(DateTime.Parse(dtg.Rows[i].Cells[5].Value.ToString()));
                        BillAddRq.DueDate.SetValue(DateTime.Parse(dtg.Rows[i].Cells[6].Value.ToString()));
                        BillAddRq.VendorRef.FullName.SetValue(vendor);
                        BillAddRq.TermsRef.FullName.SetValue(dtg.Rows[i].Cells[7].Value.ToString());
                        BillAddRq.RefNumber.SetValue(dtg.Rows[i].Cells[8].Value.ToString());
                        BillAddRq.Memo.SetValue(dtg.Rows[i].Cells[9].Value.ToString());
                        Expense = BillAddRq.ExpenseLineAddList.Append();
                        Expense.Amount.SetValue(Convert.ToDouble(amount));
                        Expense.AccountRef.FullName.SetValue(account);
                        Expense.Memo.SetValue(dtg.Rows[i].Cells[9].Value.ToString());
                        Expense.CustomerRef.FullName.SetValue(customer);
                        IMsgSetResponse responseMsgSet;
                        responseMsgSet = sessionManager.DoRequests(requestMsgSet);

                        // Uncomment the following to view and save the request and response XML
                        string requestXML = requestMsgSet.ToXMLString();
                        // Console.WriteLine(requestXML);

                        response = responseMsgSet.ResponseList.GetAt(0);
                        int statusCode = response.StatusCode;


                        // Console.WriteLine(response.StatusMessage + "   ****  " + response.StatusSeverity);

                        if (statusCode == 0)
                        {
                            dtg.Rows[i].Cells[11].Value = "Processed [x]";
                            try
                            {
                                sg.Execute("INSERT INTO mitcoltd.dbo.qb_orders_trace ([REFID] ,[EXECDATE] ,[QBstatus] ,[QBmessage]) VALUES ('" + dtg.Rows[i].Cells[10].Value.ToString() + "' ,getdate() ," + statusCode.ToString() + " ,'" + response.StatusMessage + "   ****  " + response.StatusSeverity + "' )");
                            }
                            catch (Exception)
                            {
                            }

                        }
                        else
                        {
                            dtg.Rows[i].Cells[11].Value = response.StatusMessage + "   ****  " + response.StatusSeverity;
                            try
                            {
                                sg.Execute("INSERT INTO mitcoltd.dbo.qb_orders_trace ([REFID] ,[EXECDATE] ,[QBstatus] ,[QBmessage]) VALUES ('" + dtg.Rows[i].Cells[10].Value.ToString() + "' ,getdate() ," + statusCode.ToString() + " ,'" + response.StatusMessage + "   ****  " + response.StatusSeverity + "' )");
                            }
                            catch (Exception)
                            {

                            }

                        }



                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                        //End the session and close the connection to QuickBooks
                        if (sessionBegun)
                        {
                            sessionManager.EndSession();
                        }
                        if (connectionOpen)
                        {
                            sessionManager.CloseConnection();
                        }

                        if (ex.Message.ToLower().Contains("Could not start QuickBooks.".ToLower()))
                        {
                            break;
                        }
                    }
                    finally
                    {


                        //End the session and close the connection to QuickBooks
                        if (sessionBegun)
                        {
                            sessionManager.EndSession();
                        }
                        if (connectionOpen)
                        {
                            sessionManager.CloseConnection();
                        }


                    }




                }
            }


            displayBtn.Enabled = true;
            dtg.Enabled = true;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (dtg.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "CSV (*.csv)|*.csv";
                sfd.FileName = "Download_From" + dtb.Value.ToString("yyyyMMdd") + "_To" + dte.Value.ToString("yyyyMMdd") + "_On" + DateTime.Now.ToString("yyyyMMddHHmm") + ".csv";
                bool fileError = false;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(sfd.FileName))
                    {
                        try
                        {
                            File.Delete("Exports/" + sfd.FileName);
                        }
                        catch (IOException ex)
                        {
                            fileError = true;
                            MessageBox.Show("It wasn't possible to write the data to the disk." + ex.Message);
                        }
                    }
                    if (!fileError)
                    {
                        try
                        {
                            int columnCount = dtg.Columns.Count;
                            string columnNames = "";
                            string[] outputCsv = new string[dtg.Rows.Count + 1];
                            for (int i = 0; i < columnCount; i++)
                            {
                                columnNames += dtg.Columns[i].HeaderText.ToString() + ",";
                            }
                            outputCsv[0] += columnNames;

                            for (int i = 1; (i - 1) < dtg.Rows.Count; i++)
                            {
                                for (int j = 0; j < columnCount; j++)
                                {
                                    if (dtg.Rows[i - 1].Cells[j].Value != null)
                                        outputCsv[i] += "\"" + dtg.Rows[i - 1].Cells[j].Value.ToString() + "\",";
                                    else
                                        outputCsv[i] += "" + ",";

                                }
                            }

                            File.WriteAllLines(sfd.FileName, outputCsv, Encoding.UTF8);
                            MessageBox.Show("Data Exported Successfully !!!", "Info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error :" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record To Export !!!", "Info");
            }
        }

        private void selectall_CheckedChanged(object sender, EventArgs e)
        {
            if(selectall.Checked == false)
            {
                for (int i = 0; i < dtg.Rows.Count; i++)
                {
                    dtg.Rows[i].Cells[0].Value = false;

                }
                // selectall.Checked = false;
            }
            else
            {
                for (int i = 0; i < dtg.Rows.Count; i++)
                {
                    dtg.Rows[i].Cells[0].Value = true;

                }
                // selectall.Checked = true;

            }
        }
    }
}
