﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickBooksIntegrationDemo
{
    public class InvoiceItem : ModelBase
    {
        private Item item;
        private double amount;
        private string quickBooksId;

        public Item Item 
        {
            get { return item; }
            set
            {
                if (object.ReferenceEquals(item, value) != true)
                {
                    item = value;
                    NotifyPropertyChanged("Item");
                }
            }
        }

        public double Amount 
        {
            get { return amount; }
            set
            {
                if (amount.Equals(value) != true)
                {
                    amount = value;
                    NotifyPropertyChanged("Amount");
                }
            }
        }

        public string QuickBooksID
        {
            get { return quickBooksId; }
            set
            {
                if (object.ReferenceEquals(quickBooksId, value) != true)
                {
                    quickBooksId = value;
                    NotifyPropertyChanged("QuickBooksID");
                }
            }
        }
    }
}
