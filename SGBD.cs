﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

//Version 6.1
public class SGBD
{



    public SqlConnection cn = new SqlConnection("");



    public SGBD(string db)
    {
        cn.ConnectionString = db;
    }
    //GetTableByCommande
    public DataTable getTableByCommande(string commande)
    {
        DataTable dt = new DataTable();
        try
        {
            SqlCommand cd = new SqlCommand(commande, cn);
            cd.CommandTimeout = 0;
            SqlDataAdapter d = new SqlDataAdapter(cd);
            DataSet dset = new DataSet();
            d.Fill(dset, "table");
            dt = dset.Tables["table"];
            return dt;
        }
        catch (Exception ex)
        {
            return dt;
        }
        
    }


    //Execute
    public object Execute(string commande)
    {
        //commande = (apos(commande))
        DataTable dt = new DataTable();
        SqlCommand cd = new SqlCommand(commande, cn);
        cd.CommandTimeout = 0;
        SqlDataAdapter d = new SqlDataAdapter(cd);
        DataSet dset = new DataSet();
        d.Fill(dset, "table");
        try
        {
            dt = dset.Tables["table"];
            return dt;
        }
        catch (Exception)
        {
            return -1;
        }
    }
}